import React from 'react';
import Container from '@material-ui/core/Container';
import { Input } from '@material-ui/core';
import {UsersList} from './components/UsersList';

function App() {
  const [inputValue, setInputValue] = React.useState('');

  const handleInputValue = (e) => {
    setInputValue(e.target.value);
  }

  return (
    <Container>
      <Input
        value={inputValue}
        onChange={handleInputValue}
        placeholder="Search by name or surname"
        fullWidth
      />
      <UsersList inputValue={inputValue} />
    </Container>
  );
}

export default App;
