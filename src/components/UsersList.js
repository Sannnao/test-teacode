import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';
import {Skeleton} from '@material-ui/lab';
import {UserItem} from './UserItem';

const randomSkeletonWidth = () => Math.round(Math.random() * 100) + 100;
const fakeArray = new Array(Math.round(Math.random() * 100)).fill(null);

const filteredUsers = (users, inputValue) => {
  const compareValues = (source) => source.toLowerCase().includes(inputValue.toLowerCase());

  return users.filter(user => compareValues(user.first_name) || compareValues(user.last_name))
}

export const useStyles = makeStyles({
  root: {
    marginRight: 15,
  },
  listItem: {
    '&:hover': {
      cursor: 'pointer',
    }
  }
});

export const UsersList = ({inputValue}) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = React.useState(false);
  const [users, setUsers] = React.useState(null);

  React.useEffect(
    function getUsers() {
      async function fetchUsers() {
        const rawUsers = await fetch('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json');
        const readyUsers = await rawUsers.json();
        readyUsers.sort((a, b) => {
          if (a.last_name < b.last_name) {
            return -1;
          } else if (a.last_name > b.last_name) {
            return 1;
          } else {
            return 0;
          }
        })
        const usersWithChecked = readyUsers.map(user => ({...user, checked: false}));

        setUsers(usersWithChecked);
        setIsLoading(false);
      }

      setIsLoading(true);
      fetchUsers();
    }, []
  )

  const handleChecked = (id) => {
    setUsers(prevState => {
      const copyState = [...prevState];
      const checkUserIndex = prevState.findIndex(user => user.id === id);
      copyState[checkUserIndex].checked = !copyState[checkUserIndex].checked;

      return copyState;
    })

    const checkedUsersIds = users.filter(user => user.checked).map(user => user.id);
    console.log(checkedUsersIds);
  }

  return (isLoading || !users) ? (
    <List>
      {fakeArray.map((_, i) => (
        <ListItem key={i}>
          <Skeleton className={classes.root} variant="circle" width={40} height={40} />
          <Skeleton className={classes.root} variant="text" width={randomSkeletonWidth()} />
          <Checkbox color="primary"/>
        </ListItem>
      ))}
    </List>
  ) : (
    <List>
      {filteredUsers(users, inputValue).map(user => {
        return (
          <UserItem key={user.id} user={user} handleChecked={handleChecked} />
        )
      })}
    </List>
  )
};
