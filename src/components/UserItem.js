import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Checkbox from '@material-ui/core/Checkbox';
import {useStyles} from './UsersList';

export const UserItem = ({user, handleChecked}) => {
  const classes = useStyles();
  const {id, avatar, first_name, last_name} = user;

  const toggleChecked = () => {
    handleChecked(id);
  }

  return (
    <>
      <ListItem className={classes.listItem} onClick={toggleChecked}>
        <Avatar  className={classes.root} src={avatar} alt='avatar' />
        <Typography className={classes.root}>{first_name}{' '}{last_name}</Typography>
        <Checkbox checked={user.checked} color="primary" />
      </ListItem>
      <Divider />
    </>
  )
};
